exception NoMatch

open Lwt.Infix
open Calculon

let autopan =
  let re = Re_pcre.regexp "\\bcoin\\b" in
  fun msg ->
    match Re.matches re msg.Core.message with
    | l ->
        let l = List.map (fun _ -> "pan") l in
        Lwt.return [ String.concat " " l ]
    | [] ->
        raise NoMatch

let cmd_hello : Command.t =
  Command.make ~descr:"autopan" ~name:"autopan" ~prio:10
    (fun (module C : Core.S) msg ->
      try
        let fut =
          autopan msg >>= fun lines ->
            C.send_notice_l ~target:(Core.reply_to msg) ~messages:lines
        in
        Cmd_match fut
      with NoMatch ->
        Cmd_skip
    )

let plugin = Plugin.of_cmd cmd_hello
