open Calculon

let fortunes_file = "francifortunes"

let parse_fortunes () =
  let ic = open_in_bin fortunes_file in
  let q = Queue.create () in
  (try
    while true do
      Queue.push (input_line ic) q
    done
  with End_of_file -> ());
  let fortunes = Queue.fold (fun (cur, fortunes) l ->
    match String.trim l with
    | "%" -> [], (List.rev cur :: fortunes)
    | l -> l :: cur, fortunes
  ) ([], []) q in
  Array.of_list (snd fortunes)

let random =
  Random.self_init ();
  (fun fortunes -> fortunes.(Random.int (Array.length fortunes)))


let cmd_hello : Command.t =
  let fortunes = parse_fortunes () in
  Command.make_simple ~descr:"fortunes" ~prefix:"fortune" ~prio:9
    (fun (input_msg:Core.privmsg) _ ->
       let who = input_msg.Core.nick in
       Lwt.return (Some (String.concat " " (random fortunes)))
    )

let plugin = Plugin.of_cmd cmd_hello
