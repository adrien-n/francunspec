open Calculon

let sp = Printf.sprintf

let quiet =
  try Sys.getenv "GEORGE_QUIET" = "true" with Not_found -> false

module AddrSet = Set.Make (
  struct
    type t = string
    let compare = compare
  end
)

type connectee_mode =
  | Voice
  | Op
  | Basic

type nick = string
type user_host = string * string

type connectee = {
  nick : nick;
  user_host : user_host option;
  first_seen : float;
  mode : connectee_mode;
  score : int Lwt_react.signal Lwt.t; (* TODO: ensure this is destroyed *)
  utc_send : bool -> unit;
}

type channel = {
  name : string;
  connectees : (string, connectee) Hashtbl.t;
}

module ConnecteeSet = Set.Make (
  struct
    type t = nick * user_host option * string (* channel.name *)
    let compare = compare
  end
)

type state = {
  tor_exit_addresses : AddrSet.t Lwt_react.signal;
  proxy_addresses : AddrSet.t Lwt_react.signal;
  channels : (string, channel) Hashtbl.t;
}

module Web = struct
  let getnameinfo ip =
    match Unix.inet_addr_of_string ip with
    | inet_addr ->
        begin
          let sockaddr = Unix.ADDR_INET (inet_addr, 0) in
          match%lwt Lwt_unix.getnameinfo sockaddr [] with
          | { Unix.ni_hostname; _ } ->
              Lwt.return ni_hostname
          | exception Not_found ->
              Lwt.return ip
        end
    | exception Failure _ -> (* not an IP so already reversed *)
        Lwt.return ip

  let get ~uri ~msg ~parse _ipset () =
    let open Cohttp in
    let open Cohttp_lwt_unix in
    let%lwt resp, body = Client.get (Uri.of_string uri) in
    let code = resp |> Response.status |> Code.code_of_status in
    Printf.eprintf "%s: fetch response code: %d\n%!" msg code;
    let%lwt body = Cohttp_lwt.Body.to_string body in
    parse body

end

module Tor = struct
  (* let list_fmap f l =
    let rec aux acc = function
      | hd :: tl ->
          begin
            match f hd with
            | Some e -> aux (e :: acc) tl
            | None -> aux acc tl
          end
      | [] ->
          List.rev acc
    in
    aux [] l *)

  let parse text =
    let open Re.Pcre in
    let rex = regexp "^ExitAddress ([^ ]+) " in
    let set = ref AddrSet.empty in
    let lines = split ~rex:(regexp "\n") text in
    let t0 = Unix.gettimeofday () in
    let%lwt _nothing =
      lines
      |> Lwt_list.iter_s (fun line ->
        match extract ~rex line with
        | [| _line; addr |] ->
            let%lwt name = Web.getnameinfo addr in
            set := !set
              |> AddrSet.add addr
              |> AddrSet.add name;
            Lwt.return_unit
        | _ ->
            Lwt.return_unit
        | exception _ ->
            Lwt.return_unit
      )
    in
    let t1 = Unix.gettimeofday () in
    Printf.eprintf "exit-nodes: %d entries (resolved in %f).\n%!" (AddrSet.cardinal !set) (t1 -. t0);
    Lwt.return !set

  let exit_addresses_uri = "https://check.torproject.org/exit-addresses" 

  let exit_addresses () =
    let trigger =
      let first = ref true in
      fun () ->
        if !first then
          Lwt.return (first := false)
        else
          Lwt_unix.sleep (3600. *. 12.)
    in
    Lwt_react.E.from trigger
    |> Lwt_react.S.fold_s (Web.get ~uri:exit_addresses_uri ~msg:"exit-nodes" ~parse) AddrSet.empty
end

module Proxies = struct
  let parse text =
    let open Re.Pcre in
    let rex = regexp "([^ ]+):" in
    let set = ref AddrSet.empty in
    let lines = split ~rex:(regexp "\n") text in
    let t0 = Unix.gettimeofday () in
    let%lwt _nothing =
      lines
      |> Lwt_list.iter_s (fun line ->
        match extract ~rex line with
        | [| _line; addr |] ->
            let%lwt name = Web.getnameinfo addr in
            set := !set
              |> AddrSet.add addr
              |> AddrSet.add name;
            Lwt.return_unit
        | _ ->
            Lwt.return_unit
        | exception _ ->
            Lwt.return_unit
      )
    in
    let t1 = Unix.gettimeofday () in
    Printf.eprintf "proxies: %d entries (resolved in %f).\n%!" (AddrSet.cardinal !set) (t1 -. t0);
    Lwt.return !set

  let addresses_uri = "http://olaf4snow.com/public/proxy.txt"

  let addresses () =
    let trigger =
      let first = ref true in
      fun () ->
        if !first then
          Lwt.return (first := false)
        else
          Lwt_unix.sleep (3600. *. 12.)
    in
    Lwt_react.E.from trigger
    |> Lwt_react.S.fold_s (Web.get ~uri:addresses_uri ~msg:"proxies" ~parse) AddrSet.empty
end

let default_state () =
  {
    channels = Hashtbl.create 10;
    tor_exit_addresses = Tor.exit_addresses ();
    proxy_addresses = Proxies.addresses ();
  }

let find_channel { channels; _ } chan =
  match Hashtbl.find channels chan with
  | chan -> chan
  | exception Not_found ->
      let chan = { name = chan; connectees = Hashtbl.create 10 } in
      Hashtbl.add channels chan.name chan;
      chan

let send_raw (module C : Core.S) message=
  C.I.send ~connection:C.connection message

module Shopping = struct
  module type Costs = sig
    type t
    val budget : t
    val zero : t
    val cost_function : string -> t
    val add : t -> t -> t
    val compare : t -> t -> int
  end
  module IRC_Modes : Costs = struct
    type t = {
      count : int;
      length : int;
    }

    let budget = {
      count = 12; (* XXX *)
      length = 400; (* XXX *)
    }

    let zero = {
      count = 0;
      length = 0;
    }

    let cost_function nick = {
      count = 1;
      length = String.length "v" + String.length nick;
    }

    let add a b = {
      count = a.count + b.count;
      length = a.length + b.length;
    }

    let compare a b =
      max (compare a.count b.count) (compare a.length b.length)
  end

  let bucket ~costs:(module Costs : Costs) ~lst =
    let rec aux acc current_cost = function
      | (hd :: tl) as l ->
          let cost = Costs.add current_cost (Costs.cost_function hd) in
          if Costs.compare cost Costs.budget > 0 then
            acc, l
          else
            aux (hd :: acc) cost tl
      | [] as tl ->
          acc, tl
    in
    aux [] Costs.zero lst

  let spread ~costs:(module Costs : Costs) ~lst =
    let rec aux acc lst =
      let bucket, rest = bucket ~costs:(module Costs) ~lst in
      let acc' = bucket :: acc in
      if rest = [] then
        acc'
      else
        aux acc' rest
    in
    aux [] lst
end

let voice (module C : Core.S) ~chan =
  let connectees = Hashtbl.fold (fun _k c acc -> if c.mode = Basic then c.nick :: acc else acc) chan.connectees [] in
  Shopping.(spread ~costs:(module IRC_Modes : Costs) ~lst:connectees)
  |> Lwt_list.iter_s (fun nicks ->
      let n = List.length nicks in
      let plus_v = "+" ^ (String.make n 'v') in
      Printf.fprintf stdout "Voicing\n%a\n%!" (fun outchan l -> List.iter (Printf.fprintf outchan "  %S") l) nicks;
      Irc_message.other ~cmd:"mode" ~params:(chan.name :: plus_v :: nicks)
      |> send_raw (module C : Core.S)
  )

let spp_string_option ?(prefix="") () = function
  | Some s -> sp "%s%S" prefix s
  | None -> ""

let spp_string_list () l =
  String.concat " " (List.map String.escaped l)

let pp_string_couple_option outchan = function
  | Some (s1, s2) -> Printf.fprintf outchan "%S/%S" s1 s2
  | None -> Printf.fprintf outchan "<none/none>"

let spp_string_couple_option () = function
  | Some (s1, s2) -> sp "%S/%S" s1 s2
  | None -> sp "<none/none>"

let spp_mode () = function
  | Op -> "@"
  | Voice -> "+"
  | Basic -> "."

let report_channel = "#shit-george-says"

let time_passing_by, time_passing_by_send = Lwt_react.E.create ()
let startup_time = Unix.gettimeofday () (* XXX: this is not NTP-safe *)

let mana_burn, mana_burn_send = Lwt_react.E.create ()
let ups, ups_send = Lwt_react.E.create ()

let etat_d_urgence, etat_d_urgence_send = Lwt_react.S.create 1

let messages, messages_send = Lwt_react.E.create ()

(* let vichytipiak =
  let open React in
  let level_min = 0. in
  let level_max = 5. in
  let events = E.select [
    E.map (fun () -> - 0.01) time_passing_by;
    E.map (fun _ -> prerr_endline "received message"; 0.25) messages;
    E.map (fun x -> float (x / (abs x))) mana_burn;
  ]
  in
  let f x y =
    let v' = x +. y in
    if v' < level_min then
      level_min
    else if v' > level_max then
      level_max
    else
      v'
  in
  S.fold f level_min events
  (* |> S.map ~eq:(=) (fun x -> Printf.eprintf "Vichytipiak: %f.\n%!" x; x) *)
  |> S.Float.truncate *)

let mode_change (module C : Core.S) ~chan ~mode ~args =
  Irc_message.other ~cmd:"mode" ~params:(chan :: mode :: args)
  |> send_raw (module C : Core.S)

let fmay f e =
  match f with
  | Some f -> f e
  | None -> ()

let mode_change_temporarily (module C : Core.S) ~chan ~duration ~mode ~args ?message ?nick ?on_off_hook () =
  let f on =
    let mode = (if on then "+" else "-") ^ mode in
    (if not on then fmay on_off_hook ());
    let%lwt _report =
      let message = sp "On %s: %s %a%a (%d seconds)" chan mode spp_string_list args (spp_string_option ~prefix:" for ") nick duration in
      C.send_privmsg ~target:report_channel ~message
    in
    (if not quiet then
      match message, nick with
      | Some message, Some nick -> ignore @@ C.send_notice
          ~target:nick
          ~message:(message on)
      | _ -> ()
    else
      ());
    mode_change (module C : Core.S) ~chan ~mode ~args
  in
  Lwt_timeout.(start @@ create duration (fun () -> ignore @@ f false));
  f true

let ban_broad_temporarily (module C : Core.S) ~duration ~what ~chan =
  let mask = match what with
  | `Nick s -> sp "%s!*@*" s
  | `User s -> sp "*!%s@*" s
  | `Host s -> sp "*!*@%s" s
  in
  mode_change_temporarily (module C : Core.S) ~duration ~mode:"b" ~args:[ mask ] ~chan ()

let ban_temporarily (module C : Core.S) ~duration ~chan ~nick ~user_host ~on_off_hook =
  Printf.eprintf "--- Banning %s for %d seconds!%a.\n" nick duration pp_string_couple_option user_host;
  let mask =
    let open Printf in
    match nick, user_host with
    | _, Some (user, host) -> sprintf "*!%s@%s" user host
    | nick, None -> sprintf "%s!*@*" nick
  in
  let message on =
    if on then
      sp "You will be able to talk on %s in %d seconds. / Vous pourrez parler sur %s dans %d secondes." chan duration chan duration
    else
      "You can now talk. Sorry for the disturbance. / Vous pouvez maintenant parler, la RATP vous prie de l'excuser pour la gêne occasionée."
  in
  mode_change_temporarily (module C : Core.S) ~duration ~mode:"b" ~args:[ mask ] ~message ~nick ~on_off_hook ~chan ()

let warn_if_extra_args (module C : Core.S) ~who ~extra_args =
  if extra_args <> [] then
    C.send_privmsg
      ~target:who
      ~message:"This command takes no additional arguments, ignoring them."
  else
    Lwt.return_unit

let pastille (module C : Core.S) ~chan =
  Hashtbl.fold (fun _k d acc -> d :: acc) chan.connectees []
  |> Lwt_list.iter_s (fun { nick; user_host; first_seen; mode; score; _ } ->
      let%lwt score = score in
      Lwt_io.fprintf
        Lwt_io.stdout
        "%a%S, %a, %f, %d\n"
        spp_mode mode nick spp_string_couple_option user_host first_seen (React.S.value score)
  )

let loop =
  let re_space = Re.Pcre.regexp " " in
  fun (module C : Core.S) ~state ~msg ->
    let message = msg.Core.message in
    let who = msg.Core.nick in
    let chan = msg.Core.to_ in
    match Re.Pcre.split ~rex:re_space message with
    | "!nouvelle_star" :: extra_args
    | "!voice_all" :: extra_args ->
        Command.Cmd_match (
          let%lwt () = warn_if_extra_args (module C) ~who ~extra_args in
          voice (module C : Core.S) ~chan:(find_channel state chan)
        )
    | "!mana_burn" :: v :: extra_args ->
        Command.Cmd_match (
          let%lwt () = warn_if_extra_args (module C) ~who ~extra_args in
          let mana = try int_of_string v with _ -> 100 in
          Lwt.return (mana_burn_send mana)
        )
    | "!ups" :: victim :: extra_args ->
        Command.Cmd_match (
          let%lwt () = warn_if_extra_args (module C) ~who ~extra_args in
          Lwt.return (ups_send victim)
        )
    | "!pastille" :: chan :: extra_args ->
        Command.Cmd_match (
          let chan = find_channel state chan in
          let%lwt () = pastille (module C : Core.S) ~chan in
          warn_if_extra_args (module C) ~who ~extra_args
        )
    | "!pastille" :: extra_args ->
        Command.Cmd_match (
          let chan = find_channel state chan in
          let%lwt () = pastille (module C : Core.S) ~chan in
          warn_if_extra_args (module C) ~who ~extra_args
        )
    | "!voicivenuletemps" :: multiplier :: duration :: extra_args ->
        Command.Cmd_match (
          let chan = find_channel state chan in
          let duration = 60 * (try int_of_string duration with _ -> 1) in
          let multiplier = try int_of_string multiplier with _ -> 1 in
          let report =
            sp
              "On %s: increasing etat_d_urgence to %d for %d seconds."
              chan.name
              multiplier
              duration
          in
          etat_d_urgence_send multiplier;
          ignore @@ C.send_privmsg ~target:report_channel ~message:report;
          Lwt_timeout.(start @@ create duration (fun () ->
            let report = sp "On %s: etat_d_urgence back to 1." chan.name in
            ignore @@ C.send_privmsg ~target:report_channel ~message:report;
            etat_d_urgence_send 0
          ));
          warn_if_extra_args (module C) ~who ~extra_args
        )
    | _ ->
        Command.Cmd_skip

let ban_duration_base { Unix.tm_hour = hour; _ } =
  (* Trolls are more likely to appear at night *)
  let minutes =
    if hour >= 6 && hour < 11 then 1
    else if hour >= 11 && hour < 15 then 2
    else if hour >= 15 && hour < 22 then 5
    else if hour >= 22 && hour < 24 then 8
    else 15
  in minutes * 60

let ban_duration ?(score_delta = 0) () =
  ban_duration_base Unix.(localtime @@ time ())
  |> ( * ) (truncate (1. +. log10 (1. +. (float score_delta) /. 10.)))
  |> ( * ) (1 + React.S.value etat_d_urgence)

let shall_ban ~nick ~score ~score' =
  (if score' < score then Printf.eprintf "Score for %S: %d -> %d\n%!" nick score score');
  if score >= 0 && score' < 0 then
    ban_duration ~score_delta:(-score') ()
  else
    0

let shall_ban_broad bans =
  let limit = 3 in
  let pred (_, n) = n >= limit in
  let filter_tag tag l =
    l |> List.filter pred |> List.map (fun s -> tag (fst s))
  in
  let store l e =
    let rec f l e acc =
      match l with
      | (s, n) :: tl when s = e -> (s, n + 1) :: tl 
      | sn :: tl -> f tl e (sn :: acc)
      | [] -> (e, 1) :: acc
    in
    f l e []
  in
  let nicks, users, hosts =
    ConnecteeSet.fold (fun (nick, user_host, chan) (nicks, users, hosts) ->
      let nicks = store nicks (nick, chan) in
      let users, hosts =
        match user_host with
        | Some (user, host) -> store users (user, chan), store hosts (host, chan)
        | None -> users, hosts
      in
      nicks, users, hosts
    ) bans ([], [], []) 
  in
  List.flatten [
    filter_tag (fun (s, chan) -> chan, `Nick s) nicks;
    filter_tag (fun (s, chan) -> chan, `User s) users;
    filter_tag (fun (s, chan) -> chan, `Host s) hosts
  ]

let string_ends_with ~s ~q =
  let open String in
  try sub s (length s - length q) (length q) = q with
  | Invalid_argument _ -> false

let ip_of_numeric num =
  assert (num < (256 * 256 * 256 * 256));
  let ip_1 = num / (256 * 256 * 256) in
  let ip_2 = (num / (256 * 256)) mod 256 in
  let ip_3 = (num / (256)) mod 256 in
  let ip_4 = num mod 256 in
  sp "%d.%d.%d.%d" ip_1 ip_2 ip_3 ip_4

let ip_of_kiwiirc_or_mibbit user =
  try
    let numeric = int_of_string ("0x" ^ user) in
    Some (ip_of_numeric numeric)
  with _ ->
    None

let uses_webchat_and_not_big_telco ~nick ~user_host =
  match nick, user_host with
  | _, Some ("webchat", host) ->
      not (
        string_ends_with ~s:host ~q:".abo.wanadoo.fr"
        || string_ends_with ~s:host ~q:".rev.sfr.net"
        || string_ends_with ~s:host ~q:".abo.bbox.fr"
        || string_ends_with ~s:host ~q:".proxad.net"
        || string_ends_with ~s:host ~q:".abo.nordnet.fr"
        || string_ends_with ~s:host ~q:".isp.belgacom.be"
        || string_ends_with ~s:host ~q:".voo.be"
        || string_ends_with ~s:host ~q:".scarlet.be"
        || string_ends_with ~s:host ~q:".fdn.fr"
        || string_ends_with ~s:host ~q:".coltfrance.com"
      )
  | _, Some (_, host) -> (
      string_ends_with ~s:host ~q:".mibbit.com" 
      || string_ends_with ~s:host ~q:".clients.kiwiirc.com" 
  )
 | _ -> false

let is_bad_nick = function
  | "jjlrm" | "jjlrm_" | "jjlrm__" -> true
  | _ -> false

let is_bad_user = function
  | "06.vzaker" -> true
  | _ -> false

let is_highlight =
  let rex = Re.Pcre.regexp "^[^ ]{1,20}: " in
  function msg ->
    Re.execp rex msg

let current_bans, current_bans_send =
  let change, change_send = React.E.create () in
  let eq x y =
    x = y
  in
  let s = React.S.fold ~eq (fun set (f, e) -> f e set) ConnecteeSet.empty change in
  s, change_send

let shall_ban_broad_events =
  current_bans
  |> React.S.map (fun bans -> shall_ban_broad bans)

let uses_proxy ~user:_user ~host ~proxies =
  prerr_endline host;
  AddrSet.mem host proxies

let uses_tor ~user ~host ~exit_nodes =
  if
    string_ends_with ~s:host ~q:".clients.kiwiirc.com"
    || string_ends_with ~s:host ~q:".mibbit.com"
  then
    match ip_of_kiwiirc_or_mibbit user with
    | Some ip -> AddrSet.mem ip exit_nodes
    | None -> false
  else
    AddrSet.mem host exit_nodes


let score (module C : Core.S) ~chan ~nick ~user_host ~messages ~exit_nodes ~proxies ~first_seen ~utc =
  let open Lwt_react in
  let message_score (msg_nick, msg_s) =
    if msg_nick <> nick then
      30
    else
      match msg_s with
      | "bonjour je suis jean-jacques leroy-mercier"
      | "quel est votre avis URGENT sur"
      | "bonjour quel est votre avis URGENT sur"
      | "vous connéssé" ->
          - 600
      | _ ->
          let now = Unix.gettimeofday () in
          let highlight = if is_highlight msg_s then 2. else 1. in
          let recentness = 1. +. exp (-. 0.1 *. (now -. first_seen)) in
          - truncate (0.5 +. recentness *. highlight *. (float (max (String.length msg_s) 30)))
  in
  let score_max = 2000 in
  let tor, uses_tor_send = React.S.create false in
  let proxy, uses_proxy_send = React.S.create false in
  let webchat, webchat_send = React.S.create false in
  let bad_user, bad_user_send = React.S.create false in
  let score_diff_inject, score_diff_inject_send = React.E.create () in
  let webchat_and_utc = React.S.l2 (&&) webchat utc in
  let score_diff = E.select [
    E.map message_score messages;
    E.map (fun (msg_nick, _msg_s) -> if is_bad_nick msg_nick then -600 else 0) messages;
    E.map (fun () -> if S.value webchat_and_utc || S.value tor || S.value proxy then 1 else 2) time_passing_by;
    E.map (fun _ -> -200) (S.changes tor);
    E.map (fun _ -> -200) (S.changes proxy);
    E.map (fun _ -> -100 + 30) (S.changes webchat);
    E.map (fun _ -> -300) (S.changes bad_user);
    E.map (fun _ -> -1000) (S.changes webchat_and_utc);
    score_diff_inject;
    mana_burn;
    E.map (fun victim -> if victim = nick then - 2000 else 0) ups;
  ]
  (* |> E.map (fun x -> x * (1 + S.value vichytipiak)) *)
  in
  let score_set score =
    score_diff_inject_send 0xdeadbeef;
    score_diff_inject_send (- score_max + score);
  in
  let initial_score = 200 + (if first_seen < startup_time +. 30. then 200 else 0) in
  let score = S.fold_s (fun score diff ->
    let score' = min (score + diff) score_max in
    let%lwt () =
      if diff < 0 then
        let () = Printf.eprintf "diff: %d\n%!" diff in
        let ban_duration = shall_ban ~nick ~score ~score' in
        if ban_duration > 0 then (
          let connectee = nick, user_host, chan.name in
          let on_off_hook () =
            score_set 200;
            current_bans_send (ConnecteeSet.remove, connectee)
          in
          let x =
            shall_ban_broad_events
            |> React.S.map (List.map (fun (chan, ban) ->
                prerr_endline "      something to ban !!!!!";
                ban_broad_temporarily (module C : Core.S) ~chan ~duration:600 ~what:ban
            ))
          in
          current_bans_send (ConnecteeSet.add, connectee);
          Lwt_react.S.stop x;
          ban_temporarily (module C : Core.S) ~duration:ban_duration ~chan:chan.name ~nick ~user_host ~on_off_hook
        )
        else
          Lwt.return_unit
      else
        Lwt.return_unit
    in
    Lwt.return score'
  ) initial_score score_diff
  in
  (match user_host with
  | Some (user, host) ->
      uses_tor_send (uses_tor ~user ~host ~exit_nodes);
      uses_proxy_send (uses_proxy ~user ~host ~proxies);
      bad_user_send (is_bad_user user)
  | None -> ());
  webchat_send (uses_webchat_and_not_big_telco ~nick ~user_host);
  (if first_seen >= startup_time +. 30. && (S.value webchat || S.value tor) then (
    Printf.eprintf "Attempting CTCP TIME to %S.\n%!" nick;
    ignore (C.send_privmsg ~target:nick ~message:"\001TIME\001"))
  );
  Printf.eprintf
    "user: %S [%a]: tor=%b, webchat=%b, proxy=%b\n%!"
    nick pp_string_couple_option user_host
    (S.value tor)
    (S.value webchat)
    (S.value proxy);
  Lwt.return score

let connectee (module C : Core.S) ~chan ~nick ~user_host ~mode ~exit_nodes ~proxies ~messages =
  let first_seen = Unix.gettimeofday () in
  let utc, utc_send = React.S.create false in
  let score = score (module C : Core.S) ~chan ~nick ~user_host ~messages ~first_seen ~exit_nodes ~proxies ~utc in
  { nick; user_host; first_seen; mode; score; utc_send }

module CTCP = struct
  module Time = struct
    module Answer = struct
      let is msg =
        try String.sub msg 0 6 = "\001TIME " with Invalid_argument _ -> false
        && string_ends_with ~s:msg ~q:"\001"

      let int_of_mon_abbr = function
        | "Jan" -> 0
        | "Feb" -> 1
        | "Mar" -> 2
        | "Apr" -> 3
        | "May" -> 4
        | "Jun" -> 5
        | "Jul" -> 6
        | "Aug" -> 7
        | "Sep" -> 8
        | "Oct" -> 9
        | "Nov" -> 10
        | "Dec" -> 11
        | _ -> raise (Invalid_argument "int_of_mon_abbr: unknown month abbreviation")

      let make_time ~month ~mday ~hour ~min ~sec ~year =
          let tm_mon = int_of_mon_abbr month in
          let tm_mday = int_of_string mday in
          let tm_hour = int_of_string hour in
          let tm_min = int_of_string min in
          let tm_sec = int_of_string sec in
          let tm_year = int_of_string year - 1900 in
          Printf.eprintf
            "Time: %d, %d, %d, %d, %d, %d\n%!"
            tm_mday tm_mon tm_year tm_hour tm_min tm_sec;
          fst (Unix.mktime {
            Unix.tm_sec; tm_min; tm_hour; tm_mday; tm_mon; tm_year;
            tm_wday = 0; tm_yday = 0; tm_isdst = false;
          })

      let extract_time_1 =
        (* Example: \001TIME Sun Jan 14 18:59:51 2018\001 *)
        let rex = Re.Pcre.regexp "\001TIME ... (...) (..) (..):(..):(..) (....)\001" in
        fun msg ->
          match Re.Pcre.extract ~rex msg with
          | [| _msg; month; mday; hour; min; sec; year |] ->
              make_time ~month ~mday ~hour ~min ~sec ~year
          | _ ->
              raise (Failure "extract_time_1: couldn't match time")

      let extract_time_2 =
        (* Example: \001TIME 12:00:51 6-Jan-2018\001 *)
        (* Example: \001TIME 12:00:51 26-Jan-2018\001 *)
        let rex = Re.Pcre.regexp "\001TIME (..):(..):(..) (.?.)-(...)-(....)\001" in
        fun msg ->
          match Re.Pcre.extract ~rex msg with
          | [| _msg; hour; min; sec; mday; month; year |] ->
              make_time ~month ~mday ~hour ~min ~sec ~year
          | _ ->
              raise (Failure "extract_time_2: couldn't match time")

      let extract_time_3 =
        (* Example: \001TIME Mon Aug 06 2018 17:46:13 GMT+0200 (heure d’été d’Europe centrale)\001 *)
        let rex = Re.Pcre.regexp "\001TIME ... (...) (..) (....) (..):(..):(..) GMT..... \\(.*\\)\001" in
        fun msg ->
          match Re.Pcre.extract ~rex msg with
          | [| _msg; month; mday; year; hour; min; sec |] ->
              make_time ~month ~mday ~hour ~min ~sec ~year
          | _ ->
              raise (Failure "extract_time_3: couldn't match time")

      let extract_time msg =
        try Some (extract_time_1 msg) with _ ->
          try Some (extract_time_2 msg) with _ ->
            try Some (extract_time_3 msg) with _ ->
              Printf.eprintf "Couldn't extract time from message %S.\n%!" msg;
              None

      let time_is_late_by_one_hour connectee_time =
        match connectee_time with
        | Some connectee_time ->
            let current_time = Unix.time () in
            let delta = current_time -. connectee_time in
            delta > (3300. +. 0. *. 3600.) && delta < (3900. +. 0. *. 3600.)
        | None ->
            true
    end
  end
end

module ChannelUsers = struct
  let parse_user =
    (* example: @francunspec!ocabot@80.12.58.25 *)
    let rex = Re.Pcre.regexp "[@\\+]?([^!]+)!([^@]+)@(.*)" in
    fun s ->
      match Re.Pcre.extract ~rex s with
      | [| _s; nickname; user; host |] -> Some (nickname, user, host)
      | _ -> None

  let parse_nick =
    (* example: @francunspec *)
    let rex = Re.Pcre.regexp "([@\\+])?(.*)" in
    fun s ->
      match Re.Pcre.extract ~rex s with
      | [| _s; "@"; nickname |] -> nickname, Op
      | [| _s; "+"; nickname |] -> nickname, Voice
      | [| _s; nickname |] -> nickname, Basic
      | _ -> s, Basic

  let with_parsed_user ~user ~f ~g =
    match parse_user user with
    | None -> g user
    | Some (nick, user, host) -> f ~nick ~user ~host

  let add (module C : Core.S) ~state ~chan:({ connectees; _ } as chan) ~nick ~user_host ~mode =
    let exit_nodes = React.S.value state.tor_exit_addresses in
    let proxies = React.S.value state.proxy_addresses in
    (match Hashtbl.find connectees nick with
    | x -> x
    | exception Not_found -> connectee (module C : Core.S) ~chan ~nick ~user_host ~mode ~exit_nodes ~proxies ~messages
    )
    |> Hashtbl.replace connectees nick

  let remove ~chan:{ connectees; _ } ~nick =
    match Hashtbl.remove connectees nick with
    | () -> ()
    | exception Not_found -> ()

  let handle_message (module C : Core.S) config state { Irc_message.prefix; command } =
    (* 353 RPL_NAMREPLY "<channel> :[[@|+]<nick> [[@|+]<nick> [...]]]"
     * 366 RPL_ENDOFNAMES "<channel> :End of /NAMES list" *)
    let open Irc_message in
    let g user =
      Printf.eprintf "Couldn't parse user infos %S.\n%!" user
    in
    (* let _vichytipiak_report =
      let open React in
      vichytipiak
      |> S.map ~eq:(=) (fun x -> x)
      |> S.changes
      |> E.map (fun v ->
        let message = sp "Vichytipiak level now at %d." v in
        let%lwt _report = C.send_privmsg ~target:report_channel ~message in
        Lwt.return ())
      |> Lwt_react.E.keep
    in *)
    match prefix, command with
    | _, Other ("353", _my_nick :: _at :: chan :: name_list) ->
        let chan = find_channel state chan in
        ListLabels.iter name_list ~f:(fun l ->
          Irc_helpers.split ~str:l ~c:' '
          |> List.filter ((<>) "")
          |> List.iter (fun nick ->
              let nick, mode = parse_nick nick in
              add (module C : Core.S) ~state ~chan ~nick ~user_host:None ~mode
          )
        );
        Lwt.return_unit
    | _, Other ("366", _sl) ->
        Lwt.return_unit
    | _, Other (id, sl) ->
        Printf.eprintf "NEW MESSAGE: id=%S\n%!" id;
        List.iter (Printf.eprintf "  s: %S\n%!") sl;
        Lwt.return_unit
    | Some prefix, PRIVMSG (chan, msg) when chan = config.Config.nick ->
        let message = sp "%s told me %S" prefix msg in
        C.send_privmsg ~target:report_channel ~message
    | Some prefix, PRIVMSG (_chan, msg) ->
        with_parsed_user ~user:prefix ~g ~f:(fun ~nick ~user:_user ~host:_host ->
          messages_send (nick, msg)
        );
        Lwt.return_unit
    | Some prefix, JOIN (chans, _key_list) ->
        with_parsed_user ~user:prefix ~g ~f:(fun ~nick ~user ~host ->
          let user_host = Some (user, host) in
          ListLabels.iter chans ~f:(fun chan ->
            add (module C : Core.S) ~state ~chan:(find_channel state chan) ~nick ~user_host ~mode:Basic
          )
        );
        Lwt.return_unit
    | Some _prefix (* by whom *), KICK (chans, nick, _comment) ->
        Lwt.return @@ (
          ListLabels.iter chans ~f:(fun chan ->
            Printf.eprintf "%S was kicked out of %S.\n%!" nick chan;
            remove ~chan:(find_channel state chan) ~nick
          )
        )
    | Some prefix, PART (chans, _comment) ->
        Lwt.return @@ with_parsed_user ~user:prefix ~g ~f:(fun ~nick ~user:_user ~host:_host ->
          ListLabels.iter chans ~f:(fun chan ->
            remove ~chan:(find_channel state chan) ~nick
          )
        )
    | Some prefix, QUIT _message ->
        Lwt.return @@ with_parsed_user ~user:prefix ~g ~f:(fun ~nick ~user:_user ~host:_host ->
          Hashtbl.iter
            (fun _chan_name chan -> remove ~chan ~nick)
            state.channels 
        )
    | Some prefix, NICK new_nick ->
        Lwt.return @@ with_parsed_user ~user:prefix ~g ~f:(fun ~nick ~user:_user ~host:_host ->
          Hashtbl.iter
            (fun chan_name chan ->
              Printf.eprintf "Nick change on %S: %S -> %S\n%!" chan_name nick new_nick;
              let connectee = Hashtbl.find chan.connectees nick in
              remove ~chan ~nick;
              Hashtbl.add chan.connectees new_nick connectee 
            )
            state.channels
        )
    | Some prefix, NOTICE (target, message) when target = config.Config.nick && CTCP.Time.Answer.is message ->
        Lwt.return @@ with_parsed_user ~user:prefix ~g ~f:(fun ~nick ~user:_user ~host:_host ->
          Hashtbl.iter
            (fun _chan_name chan ->
              try
                let connectee = Hashtbl.find chan.connectees nick in
                let connectee_time = CTCP.Time.Answer.extract_time message in
                connectee.utc_send (CTCP.Time.Answer.time_is_late_by_one_hour connectee_time)
              with
                Not_found -> ()
            )
            state.channels
        )
    | None, (JOIN _ | KICK _ | PART _ | QUIT _) ->
        Printf.eprintf "Meep\n%!";
        Lwt.return_unit
    | _ ->
        Printf.eprintf "Message not handled.\n%!";
        Lwt.return_unit
end

let handle_others config state (module C : Core.S) irc_message =
  Printf.eprintf "[%f] Received %S.\n%!" (Unix.gettimeofday ()) (Irc_message.to_string irc_message);
  ChannelUsers.handle_message (module C : Core.S) config state irc_message

let cmd_admin state : Command.t =
  Command.make ~descr:"admin" ~name:"admin" ~prio:10
    (fun (module C : Core.S) (msg : Core.privmsg) ->
      if msg.Core.nick = "adrien" then
        loop (module C) ~state ~msg
      else
        Command.Cmd_skip
    )

let plugin config =
  let _ = Lwt_engine.on_timer 1. true (fun _ -> time_passing_by_send ()) in
  Plugin.stateful
    ~name:"admin"
    ~commands:(fun state -> [ cmd_admin state ]) ()
    ~on_msg:(fun state -> [ handle_others config state ])
    ~to_json:(fun _state -> None)
    ~of_json:(fun _ _ -> Lwt.return (Result.Ok (default_state ())))
